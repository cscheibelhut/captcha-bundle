# Contao 4 Captcha Bundle
Stellt entweder Google ReCaptcha v2 oder die hCaptcha Alternative für Formulare zur Verfügung.

## Konfiguration
Für beide Captcha-Anbieter muss bei Verwendung im jeweiligen Account der *SiteKey* sowie das *Secret* generiert werden.
Dieses wird dann in der Bundle-Konfiguration hinterlegt, in der `contao/config.yml`:
```yaml
rapid_data_captcha:
    sitekey: '612b2149-xxxx-4ff0-xxxx-907ef3050b9b'
    secret: '0xxxxxxxxx5a21b154E2b274041ff5aC72aAda591D2'
```

Neben den Keys sind zusätzlich folgende Variablen über die config.yml konfigurierbar:
```yaml
     puzzleEndpoint: 'xxxxxxx/public/puzzle.php'
     widgetModulePath: 'xxxxxx/js/widget.module.min.js'
     widgetPath: 'xxxxxxx/js/widget.min.js'
     validationUrl: 'https://api.friendlycaptcha.com/api/v1/siteverify'
```

*__Tipp:__ Wenn die Konfiguration nicht vorhanden ist, wird automatisch versucht, `CAPTCHA_SITEKEY`, `CAPTCHA_SECRET`, `PUZZLE_ENDPOINT`, `WIDGET_MODULE_PATH`,  `WIDGET_PATH` und `VALIDATION_URL` aus der `.env` Datei zu verwenden*

Soll **Google reCaptcha** verwendet werden, muss noch die Service-Konfiguration angepasst werden (der Standard ist hCaptcha):
```yaml
services:
    RapidData\CaptchaBundle\Service\CaptchaInterface:
        alias: 'RapidData\CaptchaBundle\Service\ReCaptchaService'
        public: true
```

Soll **Friendly Captcha** verwendet werden, muss noch die Service-Konfiguration angepasst werden (der Standard ist hCaptcha):
```yaml
services:
    RapidData\CaptchaBundle\Service\CaptchaInterface:
        alias: 'RapidData\CaptchaBundle\Service\FriendlyCaptchaService'
        public: true
```

Zudem muss zusätzlich unter 'services' der http_client-Service und logger-Service eingestellt werden:
```yaml
  http_client:
    class: 'Symfony\Component\HttpClient\CurlHttpClient'
    public: true
  logger:
    alias: 'monolog.logger'
    public: true
```
