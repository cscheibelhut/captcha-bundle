<?php

declare(strict_types=1);

/*
 * This file is part of the Captcha Bundle for Contao.
 *
 * (c) Rapid Data AG
 *
 * @license LGPL-3.0-or-later
 */

namespace RapidData\CaptchaBundle\Service;

class CaptchaSettingsService implements CaptchaSettingsInterface
{
    private string $siteKey;
    private string $secret;
    private string $puzzleEndpoint;
    private string $widgetModulePath;
    private string $widgetPath;
    private string $validationUrl;

    public function __construct(array $extensionConfig)
    {
        $this->siteKey = $extensionConfig['sitekey'];
        $this->secret = $extensionConfig['secret'];
        $this->puzzleEndpoint = $extensionConfig['puzzleEndpoint'];
        $this->widgetModulePath = $extensionConfig['widgetModulePath'];
        $this->widgetPath = $extensionConfig['widgetPath'];
        $this->validationUrl = $extensionConfig['validationUrl'];
    }

    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function getPuzzleEndPoint(): string
    {
        return $this->puzzleEndpoint;
    }

    public function getWidgetModulePath(): string
    {
        return $this->widgetModulePath;
    }

    public function getWidgetPath(): string
    {
        return $this->widgetPath;
    }

    public function getValidationUrl(): string
    {
        return $this->validationUrl;
    }
}
